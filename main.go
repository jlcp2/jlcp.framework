package main

import (
	"jlcp.framework/api/router"
	"github.com/gin-gonic/gin"
	"jlcp.framework/logger"
	"time"
	"strconv"
)

func main() {
	logger.InitLogger("http://localhost:8081/v1/setApplicationLog")
	router.AddRoute(router.GET, "test", test)
	//router.InitRoutering("8090", "")

	for i := 0; i < 10000; i++{
		 conv := strconv.Itoa(i)
		logger.Error("Jlcp.Framework.Main", "Teste de envio de log na Thread " + conv, nil)
	}
	time.Sleep(2 * time.Second)
}

func test (c *gin.Context){
	logger.Error("Jlcp.Framework.Main", "Teste de envio de log", nil)
}
