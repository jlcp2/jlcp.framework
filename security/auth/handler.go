package auth


import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
	"jlcp.framework/security/claims"
	"github.com/gin-gonic/gin"
	dispatchers "jlcp.framework/api/dispatcher"
	"strconv"
)

func RequireTokenAuthentication(next gin.HandlerFunc) gin.HandlerFunc{
	return gin.HandlerFunc(func(c *gin.Context) {
		token := extractTokenFromRequest(c.Writer, c.Request)
		if token != nil{
			if token.Valid {
				next(c)
			}else{
				dispatchers.MakeJsonUnauthorized(*c, "Invalid Token. You are no authorized to perform this action.")
			}
		} else {
			dispatchers.MakeJsonUnauthorized(*c, "You are not authorized to perform this action.")
		}
	})
}

func extractTokenFromRequest(w http.ResponseWriter, r *http.Request) *jwt.Token{
	tokenString := r.Header.Get("Bearer")
	if tokenString != "" {
		token, _ := jwt.ParseWithClaims(tokenString, &claims.CustomClaims{}, func(token *jwt.Token) (interface{}, error) {

			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			authBackend := InitJWTAuthenticationBackend(token.Signature)
			return authBackend.Key, nil
		})

		if claims, ok := token.Claims.(*claims.CustomClaims); !ok && !token.Valid {
			token.Valid = false
		} else {
			includeFormData(r, claims)
		}

		return token
	}else{
		return nil
	}
}


func includeFormData(r *http.Request, claims *claims.CustomClaims){
	r.Header.Add("Accounts", strconv.Itoa(claims.UserId))
	r.Header.Add("Role", claims.Role)
}

func (backend *JWTAuthenticationStructure) getTokenRemainingValidity(timestamp interface{}) int {
	if validity, ok := timestamp.(float64); ok {
		tm := time.Unix(int64(validity), 0)
		remainer := tm.Sub(time.Now())
		if remainer > 0 {
			return int(remainer.Seconds() + expireOffset)
		}
	}
	return expireOffset
}