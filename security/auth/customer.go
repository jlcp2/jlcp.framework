package auth

import "net/http"

type customer struct{
	UserId int
	Name string
	Document int
	Email string
	Status int
	Valid bool
	Role int
	Password string
}

var Customer = customer{}

func CreateNewInstanceFromCustomer() customer{
	return Customer
}

func (c *customer) BuildCustomer(userId string, password string, w http.ResponseWriter){
	//TODO: Método que vai no banco e monta o usuário. Vou deixar chumbadinho aqui.
	Customer.UserId = 12345
	Customer.Name = "Weverton César Peres Bernardes"
	Customer.Document = 36163719883
	Customer.Email = "tom@mible.io"
	Customer.Status = 1 //1 valid, 0 invalid?
	Customer.Role = 2 //0 leitura, 1 operação, 2 admin
	Customer.Password = "SenhaGenérica"
}