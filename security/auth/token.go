package auth


import (
	jwt "github.com/dgrijalva/jwt-go"
	"time"
	"jlcp.framework/security/claims"
	"strconv"
)
type JWTAuthenticationStructure struct {
	Key []byte
}

const (
	tokenDuration = 10
	expireOffset  = 3600
)

type TokenStructure struct{
	Token string `json:"token"`
}

var authBackendInstance *JWTAuthenticationStructure = nil

func InitJWTAuthenticationBackend(userid string) *JWTAuthenticationStructure {
	if authBackendInstance == nil {
		authBackendInstance = &JWTAuthenticationStructure{
			Key: []byte(userid),
		}
	}

	return authBackendInstance
}
func (backend *JWTAuthenticationStructure) GenerateToken(c customer) (TokenStructure) {
	token := generateClaims(c)
	return TokenStructure{
		Token: singTokenString(token, backend),
	}
}

func singTokenString(token *jwt.Token, backend *JWTAuthenticationStructure) string{
	tokenString, err := token.SignedString(backend.Key)
	if err != nil {
		return ""
	}
	return tokenString
}

func generateClaims(c customer) *jwt.Token{
	claim := claims.CustomClaims{
		strconv.Itoa(c.Role),
		c.UserId,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(time.Minute * tokenDuration)).Unix(),
		},
	}
	return jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
}
