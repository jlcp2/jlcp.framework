package claims


import (
	"github.com/dgrijalva/jwt-go"
)

type CustomClaims struct {
	Role string `json:"role"`
	UserId int `json:"userId"`
	jwt.StandardClaims
}