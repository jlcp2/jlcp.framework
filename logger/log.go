package logger

import (
	"encoding/json"
	"net/http"
	"io"
)

var _url string

func InitLogger(url string){
	_url = url
}

type messag struct{
	Level 		string `json:",omitempty"`
	Origin 		string `json:",omitempty"`
	Message 	string `json:",omitempty"`
	Err 		error `json:",omitempty"`
}

func Debug(scope string, message string, err error){
	buildMessage("Debug", scope, message, err)
}

func Warning(scope string, message string, err error){
	buildMessage("Warning", scope, message, err)
}

func Info(scope string, message string, err error){
	buildMessage("Info", scope, message, err)
}
func Error(scope string, message string, err error){
	buildMessage("Error", scope, message, err)
}

func buildMessage(level string, scope string, message string, err error) {
	var e error
	if err != nil {
		e = err
	}

	m := messag{
		Level:   level,
		Origin:  scope,
		Message: message,
		Err:     e,
	}
	go func() {
			sendMessage(m, _url)
	}()
}

func sendMessage(m messag, url string){
	pr, pw := io.Pipe()
	go func() {
		err := json.NewEncoder(pw).Encode(&m)
		if err != nil{
			//panic("Failed to serialize the log message to json. " + err.Error())
		}
		pw.Close()
	}()
	_,erro := http.Post(url, "application/json", pr)
	if erro != nil{
		//panic("Failed to send message to logger endpoint. Please check your configuration file to see if the endpoint is right and check on the remote machine if service is running. " + erro.Error())
	}
}