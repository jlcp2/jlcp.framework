package dispatcher


import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func MakeBadRequest(c gin.Context, format string, params ...interface{}){
	c.String(http.StatusBadRequest, format, params)
}

func MakeUnauthorized(c gin.Context, format string, params ...interface{}){
	c.String(http.StatusUnauthorized, format, params)
}

func MakeSuccess(c gin.Context, format string, params ...interface{}){
	c.String(http.StatusOK, format, params)
}

func MakeJsonSuccess(c gin.Context, obj interface{}){
	c.JSON(http.StatusOK, obj)
}

func MakeJsonUnauthorized(c gin.Context, obj interface{}){
	c.JSON(http.StatusUnauthorized, obj)
}

func MakeJsonBadRequest(c gin.Context, obj interface{}){
	c.JSON(http.StatusBadRequest, obj)
}
