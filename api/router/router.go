package router


import (
	"github.com/gin-gonic/gin"
)

type MethodTypes int

const (
	GET 	 MethodTypes = iota
	POST
	PUT
	DELETE
)

type routes []mapper
var _routes = routes{}

type mapper struct{
	Method MethodTypes
	Pattern string
	Handler gin.HandlerFunc
}

func addRoutesToMapper(router gin.IRouter){
	for _, route := range _routes  {
		switch route.Method {
		case POST:
			router.POST(route.Pattern, route.Handler)
		case PUT:
			router.PUT(route.Pattern, route.Handler)
		case DELETE:
			router.DELETE(route.Pattern, route.Handler)
		default:
			router.GET(route.Pattern, route.Handler)
		}
	}
}


func AddRoute(method MethodTypes, pattern string, handler gin.HandlerFunc){
	_route := mapper{
		method,                     //Web method da rota
		"/"+pattern,						// Padrão da rota. Ex: /api/v1/blablabla
		handler,                    // Handler =*
	}
	_routes = append(_routes, _route)
}

func InitRoutering(port string, apiVersion string){
	router := gin.New()
	router.Use(gin.Recovery())
	version := router.Group("/"+ apiVersion)
	{
		addRoutesToMapper(version)
	}
	router.Run(":"+ port)
}

